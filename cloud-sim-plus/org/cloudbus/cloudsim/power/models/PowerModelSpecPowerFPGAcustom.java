/*
 * Title:        CloudSim Toolkit
 * Description:  CloudSim (Cloud Simulation) Toolkit for Modeling and Simulation of Clouds
 * Licence:      GPL - http://www.gnu.org/copyleft/gpl.html
 *
 * Copyright (c) 2009-2012, The University of Melbourne, Australia
 */

package org.cloudbus.cloudsim.power.models;

/**
 * The power model of an FPGA. We hypothesize that FPGA will have a constant 80% of workload. So if the FPGA is involved in the computation process,
 * it will consume 80% respect to the maximum power consumption. This is an approximation according to consumption in real cases <br/>
 * <a href="TODO: spec link">
 * TODO: spec link</a>
 *
 * <p>If you are using any algorithms, policies or workload included in the power package please cite
 * the following paper:</p>
 *
 * <ul>
 * <li><a href="http://dx.doi.org/10.1002/cpe.1867">Anton Beloglazov, and Rajkumar Buyya, "Optimal Online Deterministic Algorithms and Adaptive
 * Heuristics for Energy and Performance Efficient Dynamic Consolidation of Virtual Machines in
 * Cloud Data Centers", Concurrency and Computation: Practice and Experience (CCPE), Volume 24,
 * Issue 13, Pages: 1397-1420, John Wiley & Sons, Ltd, New York, USA, 2012</a></li>
 * </ul>
 *
 * @author Francesco Lubrano
 * @since CloudSim Toolkit 3.0
 */
public class PowerModelSpecPowerFPGAcustom extends PowerModelSpecPower {
	/**
     * The power consumption according to the utilization percentage.
     * @see #getPowerData(int)
     */
	private final double[] power = { 100, 102, 104, 107, 110, 116, 121, 125, 127, 130, 133 };

	@Override
	protected double getPowerData(int index) {
		return power[index];
	}

}
