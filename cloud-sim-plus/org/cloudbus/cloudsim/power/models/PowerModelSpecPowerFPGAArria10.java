package org.cloudbus.cloudsim.power.models;

/**
 * The power model of an FPGA. We hypothesize that FPGA will consume if used 80% of its maximum capacity. 
 * In this power model the FPGA used is an Intel Arria10-1150 GX. We can estimate a linear power model that follow this formula:
 * P = 37.97*R + 27.5
 * where P is the power consumption and R is the utilization percentage.
 * So we can assume 2 power state for the FPGA, the idle state (P=27.5 W) and the working state (P = 37.97*0.8 + 27.5 = 57.876 W).
 * In this case the FPGA accelerator is associated to a Hp ProLiant Ml110G5 Xeon 3075.
 * This is an approximation according to consumption in real cases <br/>
 *
 *<a href="http://dx.doi.org/10.1002/cpe.1867">Anton Beloglazov, and Rajkumar Buyya, "Optimal Online Deterministic Algorithms and Adaptive
 * Heuristics for Energy and Performance Efficient Dynamic Consolidation of Virtual Machines in
 * Cloud Data Centers", Concurrency and Computation: Practice and Experience (CCPE), Volume 24,
 * Issue 13, Pages: 1397-1420, John Wiley & Sons, Ltd, New York, USA, 2012</a>
 * 
 * @author Francesco Lubrano
 * @since CloudSim Toolkit 3.0
 */


public class PowerModelSpecPowerFPGAArria10 extends PowerModelSpecPower{
	/**
     * The power consumption according to the utilization percentage.
     * @see #getPowerData(int)
     */
	private final double[] power = { 93.7, 97, 101, 105, 110, 116, 121, 125, 129, 133, 135 };
	/**
	 * The FPGA power consumption estimating that if the FPGA is active it consumes always the 80% of its maximum
	 */
	private final double[] fpgaPower = { 27.5, 57.876, 57.876, 57.876, 57.876, 57.876, 57.876, 57.876, 57.876, 57.876, 57.876 };

	/**
	 * The returned power is the sum between the CPU and FPGA accelerator power consumption
	 */
	@Override
	protected double getPowerData(int index) {
		return (power[index] + fpgaPower[index]);
	}
}
