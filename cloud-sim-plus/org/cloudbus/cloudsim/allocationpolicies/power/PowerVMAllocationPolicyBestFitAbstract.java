package org.cloudbus.cloudsim.allocationpolicies.power;

import java.util.Iterator;

import org.cloudbus.cloudsim.allocationpolicies.VmAllocationPolicyAbstract;
import org.cloudbus.cloudsim.core.Simulation;
import org.cloudbus.cloudsim.hosts.Host;
import org.cloudbus.cloudsim.hosts.power.PowerHost;
import org.cloudbus.cloudsim.util.Log;
import org.cloudbus.cloudsim.vms.Vm;

public abstract class PowerVMAllocationPolicyBestFitAbstract extends VmAllocationPolicyAbstract implements PowerVmAllocationPolicy{
	
	@Override
    public boolean allocateHostForVm(Vm vm) {
        return allocateHostForVm(vm, findHostForVm(vm));
    }

    @Override
    public boolean allocateHostForVm(Vm vm, Host host) {
        final Simulation simulation = vm.getSimulation();
        if (host == PowerHost.NULL) {
            Log.printFormattedLine("%.2f: No suitable host found for VM #" + vm.getId() + "\n", simulation.clock());
            return false;
        }

        if (host.createVm(vm)) { // if vm has been successfully created in the host
            Log.printFormattedLine(
                "%.2f: VM #" + vm.getId() + " has been allocated to the host #" + host.getId(),
                simulation.clock());
            return true;
        }
        Log.printFormattedLine(
            "%.2f: Creation of VM #" + vm.getId() + " on the host #" + host.getId() + " failed\n",
            simulation.clock());
        return false;
    }

    /**
     * @author Francesco Lubrano
     * This function 
     * */
    @Override
    public PowerHost findHostForVm(Vm vm) {
    	if (vm.getFPGA_accelerator()) {
    		System.out.println("VM " + vm.getId() + " is suitable for FPGA accelerator");
    		Iterator<PowerHost> FPGAHosts =  this.<PowerHost>getHostList()
                    .stream()
                    .sorted()
                    .filter(h -> h.isSuitableForFPGAVm(vm))
                    .iterator();
    		PowerHost bestHost = PowerHost.NULL;
    		double bestRank = Integer.MAX_VALUE;
    		PowerHost currentHost;
    		double currentRank = 0;
    		while(FPGAHosts.hasNext()) {
    			currentHost = FPGAHosts.next();
    			currentRank = currentHost.getVmList().size();//currentHost.getNumberOfFreePes()/currentHost.getNumberOfPes();
    			if (currentRank == 0) {
    				bestHost = currentHost;
    				break;
    			}
    			if (currentRank < bestRank) {
    				bestRank = currentRank;
    				bestHost = currentHost;
    			}
    		}
    		System.out.println("Choosen Host #"+ bestHost.getId());
    		return bestHost;
    		
    	}
    	else {
    		System.out.println("VM " + vm.getId() + " is NOT suitable for FPGA accelerator");
    		Iterator<PowerHost> NoFPGAHosts =  this.<PowerHost>getHostList()
                    .stream()
                    .sorted()
                    .filter(h -> h.isSuitableForVm(vm))
                    .iterator();
    		PowerHost bestHost = PowerHost.NULL;
    		double bestRank = Integer.MAX_VALUE;
    		PowerHost currentHost;
    		double currentRank = 0;
    		while(NoFPGAHosts.hasNext()) {
    			currentHost = NoFPGAHosts.next();
    			//System.out.println("current host: " + currentHost.getId() +" - "+currentHost.getPeList().size());
    			currentRank = currentHost.getVmList().size();//currentHost.getNumberOfFreePes()/currentHost.getNumberOfPes();
    			if (currentRank == 0) {
    				bestHost = currentHost;
    				bestRank = currentRank;
    				break;
    			}
    			if (currentRank < bestRank) {
    				bestRank = currentRank;
    				bestHost = currentHost;
    			}
    		}
    		System.out.println("Choosen Host #"+ bestHost.getId()+ " with rank: "+String.format("%.2f",bestRank));
    		return bestHost;
    	}
    }

    @Override
    public void deallocateHostForVm(Vm vm) {
        vm.getHost().destroyVm(vm);
    }
}
