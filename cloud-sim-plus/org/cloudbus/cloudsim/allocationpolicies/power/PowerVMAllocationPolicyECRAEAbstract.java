/*
 * Title:        CloudSim Toolkit
 * Description:  CloudSim (Cloud Simulation) Toolkit for Modeling and Simulation of Clouds
 * Licence:      GPL - http://www.gnu.org/copyleft/gpl.html
 *
 * Copyright (c) 2009-2012, The University of Melbourne, Australia
 */

package org.cloudbus.cloudsim.allocationpolicies.power;

import org.cloudbus.cloudsim.hosts.Host;
import org.cloudbus.cloudsim.hosts.power.PowerHost;
import org.cloudbus.cloudsim.util.Log;
import org.cloudbus.cloudsim.vms.Vm;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.cloudbus.cloudsim.allocationpolicies.VmAllocationPolicyAbstract;
import org.cloudbus.cloudsim.core.Simulation;

/**
 * An abstract power-aware VM allocation policy.
 * <b>It's a policy that finds a suitable host for the vm according to the ECRAE allocation algorithm.</b>
 *
 * <p>If you are using any algorithms, policies or workload included in the power package please cite
 * the following paper:
 * <ul>
 * <li><a href="http://dx.doi.org/10.1002/cpe.1867">Anton Beloglazov, and Rajkumar Buyya, "Optimal Online Deterministic Algorithms and Adaptive
 * Heuristics for Energy and Performance Efficient Dynamic Consolidation of Virtual Machines in
 * Cloud Data Centers", Concurrency and Computation: Practice and Experience (CCPE), Volume 24,
 * Issue 13, Pages: 1397-1420, John Wiley & Sons, Ltd, New York, USA, 2012</a>
 * </ul>
 * </p>
 *
 * @author Anton Beloglazov
 * @since CloudSim Toolkit 3.0
 */
public abstract class PowerVMAllocationPolicyECRAEAbstract extends VmAllocationPolicyAbstract implements PowerVmAllocationPolicy {

    @Override
    public boolean allocateHostForVm(Vm vm) {
        return allocateHostForVm(vm, findHostForVm(vm));
    }

    @Override
    public boolean allocateHostForVm(Vm vm, Host host) {
        final Simulation simulation = vm.getSimulation();
        if (host == PowerHost.NULL) {
            Log.printFormattedLine("%.2f: No suitable host found for VM #" + vm.getId() + " FPGA:" + vm.getFPGA_accelerator() + "\n", simulation.clock());
            return false;
        }

        if (host.createVm(vm)) { // if vm has been successfully created in the host
            Log.printFormattedLine(
                "%.2f: VM #" + vm.getId() + " has been allocated to the host #" + host.getId() + " MIPS: " + host.getMips(),
                simulation.clock());
            System.out.println(vm.getId() + " has been allocated to the host #" + host.getId() + " MIPS: " + host.getMips());
            return true;
        }
        Log.printFormattedLine(
            "%.2f: Creation of VM #" + vm.getId() + " on the host #" + host.getId() + " failed\n",
            simulation.clock());
        return false;
    }

    /**
     * @author Francesco Lubrano
     * @description
     * This function find a suitable host for the vm according to the ECRAE allocation algorithm.
     * If a VM support FPGA accelerator, then a list of host with FPGA will be returned. Then to this list is applied the ECRAE algorithm 
     * (OPERA deliverable D5.4).   
     * */
    @Override
    public PowerHost findHostForVm(Vm vm) {
    	
    	if (vm.getFPGA_accelerator()) {

    		System.out.println("VM " + vm.getId() + " is suitable for FPGA accelerator. MIPS: " + vm.getTotalMipsCapacity());
    		PowerHost FPGAHost;
    		Iterator<PowerHost> FPGAActiveHostList =  this.getFPGAHostStreamList(vm)
    				.filter(h -> !h.getVmList().isEmpty()).iterator();
    		
    		if (FPGAActiveHostList.hasNext()) {
    			//at least one host active and with FPGA support
    			System.out.println("At least one host active and with FPGA support");
    			FPGAHost = calculateRanking(FPGAActiveHostList, vm);
    		}else {
    			Iterator<PowerHost> FPGAHostList =  this.getFPGAHostStreamList(vm).iterator();
    			FPGAHost = calculateRanking(FPGAHostList, vm);
    		}
    		
    		if (FPGAHost.equals(PowerHost.NULL)) {
    			Iterator<PowerHost> NoFPGAHostList =  this.getHostStreamList(vm).iterator();        
    			return calculateRanking(NoFPGAHostList, vm);
    		}else 
    			return FPGAHost;
    	
    	}else {
    		System.out.println("VM " + vm.getId() + " is NOT suitable for FPGA accelerator. MIPS: " + vm.getTotalMipsCapacity());
    		Iterator<PowerHost> NoFPGAHostList =  this.getHostStreamList(vm).iterator();
    		Iterator<PowerHost> NoFPGAActiveHostList =  this.getHostStreamList(vm).filter(h -> !h.getVmList().isEmpty()).iterator();
			if (NoFPGAActiveHostList.hasNext()) {
				System.out.println("Active");
				return calculateRanking(NoFPGAActiveHostList, vm);
			}else {
				System.out.println("No Active");
				return calculateRanking(NoFPGAHostList, vm);
			}
    	}
    }

    private Stream<PowerHost> getHostStreamList (Vm vm){
    	return this.<PowerHost>getHostList()
        .stream()
        .sorted()
        .filter(h -> h.isSuitableForVm(vm));
    }
    
    private Stream<PowerHost> getFPGAHostStreamList (Vm vm){
    	return this.<PowerHost>getHostList()
        .stream()
        .sorted()
        .filter(h -> h.isSuitableForFPGAVm(vm));
    }
    
    public PowerHost calculateRanking(Iterator<PowerHost> suitableHostList, Vm vm) {
    	System.out.println("Ranking function");
    	HashMap<PowerHost, Float> rankingMap = new HashMap<PowerHost, Float>();
    	
    	DecimalFormat df = new DecimalFormat("##.####");
    	df.setRoundingMode(RoundingMode.UP);
    	
    	while(suitableHostList.hasNext()) {
    		PowerHost host = suitableHostList.next();
    		double actualPowerConsumption = host.getPower();
    		float CPULoadIncrement = (float)vm.getProcessor().getCapacity()/(float)host.getPeList().size();
    		float ranking = (float) (CPULoadIncrement * actualPowerConsumption);
    		rankingMap.put(host, ranking);
    		System.out.println("Power: " + actualPowerConsumption + "| cpu increment: "+ df.format(CPULoadIncrement) + "| Ranking: " +ranking);
    	}
    	//find the host with best rank or return PowerHost.NULL
    	if(!rankingMap.isEmpty()) 
    		return rankingMap.entrySet().stream().min((entry1, entry2) -> entry1.getValue() > entry2.getValue() ? 1 : -1).get().getKey(); 
    	else
    		return PowerHost.NULL;
    }
    
    @Override
    public void deallocateHostForVm(Vm vm) {
        vm.getHost().destroyVm(vm);
    }
}
