package Test1;
import java.io.IOException;

import org.cloudsimplus.builders.tables.CloudletsTableBuilder;


public class Test1 {
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public static void main(String[] args) throws IOException {
		String inputFolder = "";
		String outputFolder = "C:\\Users\\Alessio-ISMB\\Desktop\\Opera\\cloud_sim";
		String workload = "random"; // Random workload
		String vmAllocationPolicy = "bestFit";
		String vmSelectionPolicy = "rs";

		new Test1Runner(
            true,//enable output
            true,//output to file
				inputFolder,
				outputFolder,
				workload,
				vmAllocationPolicy,
				vmSelectionPolicy,
				0);
	}
}
